<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('payment_token');
            $table->tinyInteger('user_id');
            $table->string('plan_id');
            
            $table->string('start_date');
            $table->string('end_date');

            $table->string('start_time');
            $table->string('end_time');

            $table->tinyInteger('current_takes');
            $table->tinyInteger('total_takes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription');
    }
}
