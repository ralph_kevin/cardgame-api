<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameResultPickedAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_result_picked_answers', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('result_id');
            $table->tinyInteger('picked_id');
            $table->string('reason_en');
            $table->string('reason_cn');
            $table->string('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_result_picked_answers');
    }
}
