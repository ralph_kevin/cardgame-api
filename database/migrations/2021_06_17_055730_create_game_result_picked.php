<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameResultPicked extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_result_picked', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('result_id');
            $table->tinyInteger('pick_id');
            $table->string('name_cn');
            $table->string('name_en');
            $table->string('description_cn');
            $table->string('description_en');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_result_picked');
    }
}
