<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();
            $table->string('plan_id');
            $table->tinyInteger('total_months');
            $table->tinyInteger('hkd');
            $table->string('length_description');
            $table->string('plan_descripton');
            $table->tinyInteger('takes');
            $table->timestamps();
        });

        DB::table('plans')->insert(
            array(
                [
                    'plan_id' => '1yrplan',
                    'total_months' => 12,
                    'hkd' => 42,
                    'length_description' => '1 Year Plan',
                    'plan_descripton' => 'Eligible for four (4) takes only and license valid for one year',
                    'takes' => 4,
                ],
                [
                    'plan_id' => '2yrplan',
                    'total_months' => 24,
                    'hkd' => 70,
                    'length_description' => '2 Years Plan',
                    'plan_descripton' => 'Eligible for four (4) takes only and license valid for one year',
                    'takes' => 4,
                ],
                [
                    'plan_id' => '3yrplan',
                    'total_months' => 36,
                    'hkd' => 90,
                    'length_description' => '3 Years Plan',
                    'plan_descripton' => 'Eligible for four (4) takes only and license valid for one year',
                    'takes' => 4,
                ],
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
