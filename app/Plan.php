<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = ['plan_id', 'total_months', 'hkd', 'length_description', 'plan_description', ];

    //protected $hidden =['title'];
}
