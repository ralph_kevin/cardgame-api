<?php

namespace App\Http\Controllers\Game;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Route;
use \App\User;

class GameController extends Controller
{
    public function getLastResults($userId){
        $results = [];
        $data = DB::table('game_result')->where('user_id', $userId)->orderBy('id', 'desc')->limit(1)->get();
        foreach($data as $item){
            $result = $this->getResults($item->result_id);
            array_push($results, $result);
        }

        if(count($results) == 1){
            return [
                'success' => true,
                'data' => $results[0]
            ];
        } else {
            return [
                'success' => false,
                'data' => ''
            ];
        }
    }

    public function getAllResults($userId){
        $results = [];
        $data = DB::table('game_result')->where('user_id', '=', $userId)->orderBy('id', 'asc')->get();
        foreach($data as $item){
            $result = $this->getResults($item->result_id);
            array_push($results, $result);
        }
        return $results;
    }

    public function getResults($resultId){
     
        $data = DB::table('game_result')->where('result_id', '=', $resultId)->get();
        if(count($data) <= 0){
            return response(['message'=>'Result is not exist!'], 400);
        }

        // user
        $user_id = $data[0]->user_id;
        $cuser = User::find($user_id);

        // result
        $dataResult = DB::table('game_results')->where('result_id', '=', $resultId)->orderBy('index', 'asc')->get();

        // picked
        $cards = [];
        $dataPicked = DB::table('game_result_picked')->where('result_id', '=', $resultId)->orderBy('id', 'asc')->get();
        $dataPicked = json_decode(json_encode($dataPicked), true);
        foreach($dataPicked as $picked){

            //reason
            $dataReasons = DB::table('game_result_picked_reason')->where('result_id', '=', $resultId)->where('picked_id', '=', $picked['pick_id'])->orderBy('id', 'asc')->get();

            //answers
            $dataAnswers = DB::table('game_result_picked_answers')->where('result_id', '=', $resultId)->where('picked_id', '=', $picked['pick_id'])->orderBy('id', 'asc')->get();

            array_push($cards, array(
                "id" => $picked['pick_id'],
                "name_cn" => $picked['name_cn'],
                "description_cn" => $picked['description_cn'],
                "name_en" => $picked['name_en'],
                "description_en" => $picked['description_en'],
                "reason" => $dataReasons,
                "answers" => $dataAnswers
            ));
        }

        $result = [
            'user' => $cuser,
            'details' => $data,
            'result' => $dataResult,
            'picked' => $cards,
        ];

        return $result;
    }

    public function result(Request $request){
        $result_id = sha1(time());
        $data = $request->json()->all();

        $this->addGameResult($result_id, $data['user_id']);
        $this->addGameResults($result_id, $data['result'] );

        $picked = [];
        foreach($data['picked'] as $item) {
            array_push($picked, array(
                "result_id" => $result_id,
                "pick_id" => $item['id'],
                "name_cn" => $item['name_cn'],
                "description_cn" => $item['description_cn'],
                "name_en" => $item['name_en'],
                "description_en" => $item['description_en'],
            ));
            $this->addResultPickedReasons($result_id, $item['id'], $item['reason']);
            $this->addResultPickedAnswers($result_id, $item['id'], $item['answers']);
        }
        $this->addResultPicked($picked);

        // update takes on plan
        $data_sub = DB::table('subscription')->where('user_id', '=', $data['user_id'])->orderBy('id', 'desc')->get();
        $current_takes = $data_sub->first()->current_takes;
        if($current_takes >= 1){
            $new_takes = $current_takes - 1;
            DB::table('subscription')
                ->where('user_id', '=', $data['user_id'])
                ->update(['current_takes' => $new_takes]);
        }

        return $result_id;
    }

    public function addGameResult($result_id, $user_id){
        DB::table('game_result')->insert(
            [
                "result_id" => $result_id,
                "user_id"   =>  $user_id,
                "json"      =>  '',
                "save_datetime" => date('Y-m-d H:i:s'),
            ]
        );
    }

    public function addGameResults($result_id, $data){
        $results = [];
        foreach($data as $item) {
            array_push($results, array(
                "result_id" => $result_id,
                "index" => $item['index'],
                "number" => $item['number'],
                "type" => $item['type'],
                "names" => $item['names'],
                "total" => $item['total']
            ));
        }
        DB::table('game_results')->insert($results);
    }

    public function addResultPicked($data){
        DB::table('game_result_picked')->insert($data);
    }
    
    public function addResultPickedReasons($result_id, $picked_id, $data){
        $reasons = [];
        foreach($data as $reason){
            $type = $reason['type'];
            if(is_array($type)){
                $type = join(",",$type);
            }

            array_push($reasons, array(
                "result_id" => $result_id,
                "picked_id" => $picked_id,
                "reason_en" => $reason['reason_en'],
                "reason_cn" => $reason['reason_cn'],
                "type" => $type,
            ));
        }
        DB::table('game_result_picked_reason')->insert($reasons);
    }

    public function addResultPickedAnswers($result_id, $picked_id, $data){
        $reasons = [];
        foreach($data as $reason){
            $type = $reason['type'];
            if(is_array($type)){
                $type = join(",",$type);
            }

            array_push($reasons, array(
                "result_id" => $result_id,
                "picked_id" => $picked_id,
                "reason_en" => $reason['reason_en'],
                "reason_cn" => $reason['reason_cn'],
                "type" => $type,
            ));
        }
        DB::table('game_result_picked_answers')->insert($reasons);
    }
}
