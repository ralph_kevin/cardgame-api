<?php

/**
 * @OA\Post(
 * path="/login",
 * summary="Authentication",
 * description="Login by email, password",
 * operationId="authLogin",
 * tags={"auth"},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       required={"email","password"},
 *       @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
 *       @OA\Property(property="password", type="string", format="password", example="PassWord12345"),
 *       @OA\Property(property="persistent", type="boolean", example="true"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 */

namespace App\Http\Controllers\Accounts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request){
        $generateCode = strval(rand(1000,9000));
        $generateToken = sha1(time());
        $request->request->add([
            'verified' => 'false',
            'verification_code' => $generateCode,
            'api_token' => $generateToken,
            'fullname' => '',
            'organization' => '',
            'description' => '',
            'photo' => ''
        ]);
        $validatedData = $request->validate([
            'username'=>'required|max:15|unique:users',
            'email'=>'email|required|unique:users',
            'password'=>'required|max:15|min:8|confirmed',
            'verified'=>'required',
            'verification_code'=>'required',
            'api_token' => 'nullable',
            'fullname' => 'nullable',
            'organization' => 'nullable',
            'description' => 'nullable',
            'photo' => 'nullable'
        ]);
        $validatedData['password'] = bcrypt($request->password);
        $user = User::create($validatedData);
        // $accessToken = $user->createToken('authToken')->accessToken;
        // return response(['user'=> $user, 'access_token'=> $accessToken]);

        $this->sendEmailVerification($request->email,$generateCode, $request->username);

        return response(['user'=> $user, 'access_token'=> $generateToken]);
    }
    
    public function login(Request $request){
        $loginData = $request->validate([
            'email'=>'required',
            'password'=>'required',
        ]);
        if(!auth()->attempt($loginData)){
            return response(['message'=>'Incorrect Email or Password!'], 400);
        }
        // $accessToken = auth()->user()->createToken('authToken')->accessToken;
        // return response(['user'=> auth()->user(), 'access_token'=> $accessToken]);
        return response(['user'=> auth()->user(), 'access_token'=> auth()->user()->api_token]);
    }

    public function verify(Request $request){
        $user = User::where('email', $request->email)->get();

        if(!$user){
            return response(['message'=>'Incorrect Email!'], 400);
        } else if($user[0]->verification_code !== $request->verification_code){
            return response(['message'=>'Incorrect verification code!'], 400);    
        } else {
            $cuser = User::find($user[0]->id);
            $cuser->verified = 'true';
            $cuser->save();
            $this->sendEmailRegistration($request->email);
            return $cuser;
        }
    }

    public function updateUser(Request $request){
        $token = $request->header('Authorization');
        $id = $request->id;

        if(!$this->isTokenExist($token, $id)){
            return response(['message'=>'Invalid user token!'], 400);
        }

        $validatedData = $request->validate([
            'email'=>'email|required',
            'fullname' => 'required',
            'organization' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time().'.'.$request->image->extension();  
        $request->image->move(public_path('images'), $imageName);
        $uploadedPath = 'public/images/'.$imageName;

        $user = User::where('id', $id)->get();
        $cuser = User::find($user[0]->id);
        $cuser->fullname = $request->fullname;
        $cuser->email = $request->email;
        $cuser->organization = $request->organization;
        $cuser->description = $request->description;
        $cuser->photo = $uploadedPath;
        $cuser->save();

        return $cuser;
    }

    public function isTokenExist($token, $id){
        $user = User::where('api_token', $token)->get();
        if(sizeof($user) <= 0){
            return false;
        } else {
            if($user[0]->id == $id){
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public function resendEmail(Request $request){
        $email = $request->email;
        $user = User::where('email', $email)->get();
        $cuser = User::find($user[0]->id);
        
        $generateCode = strval(rand(1000,9000));
        $cuser->verification_code = $generateCode;
        $cuser->save();

        $this->sendEmailVerification($email, $cuser->verification_code, $cuser->username);
        return 'Success';
    }

    public function sendEmailVerification($email, $verification_code, $name){
        $data = array(
            'name' => $name,
            'email' => $email,
            'verification_code' => $verification_code
        );
        Mail::send('mail_verify', $data, function ($message) use ($data)  {
            $message->to($data['email'], $data['email'])->subject('Find Life Interest Verification');
            $message->from('cardgame@cardgame.com','Find Life Interest');
        });
    }

    public function sendEmailRegistration($email){
        $data = array(
            'email' => $email,
        );
        Mail::send('mail_registration', $data, function ($message) use ($data)  {
            $message->to($data['email'], $data['email'])->subject('Find Life Interest Registration');
            $message->from('cardgame@cardgame.com','Find Life Interest');
        });
    }

    public function getUser($userId){
        $cuser = User::find($userId);
        if(!$cuser){
            return response(['message'=>'User does not exist!'], 400);
        }
        return $cuser;
    }

    public function changePassword(Request $request){
        $token = $request->header('Authorization');
        $id = $request->id;

        if(!$this->isTokenExist($token, $id)){
            return response(['message'=>'Invalid user token!'], 400);
        }

        $validatedData = $request->validate([
            'current_password'=>'required|max:15',
            'password'=>'required|max:15|confirmed',
        ]);

        $data = DB::table('users')
        ->where('id', $id)
        ->limit(1)->get();
        
        if(count($data) <= 0){
            return response(['message'=>'Invalid user id!'], 400);
        }
        

        if (Hash::check($request->current_password, $data[0]->password)) {
            $new_password = bcrypt($request->password);
            $update = DB::table('users')
                ->where('id', $id)
                ->limit(1)
                ->update([
                    'password' => $new_password,
                ]);

            return $this->getUser($id);
        } else {
            return response(['message'=>'Incorrect password!'], 400);
        }
    }
    
}
