<?php

namespace App\Http\Controllers\Plans;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\Plan;
use \App\User;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

class PlansController extends Controller
{
    public function index(){
        $plan = Plan::all();
        return [
            'data' => $plan->all()
        ];
    }

    public function isSubscribed($userId){
        $cuser = User::find($userId);
        if(!$cuser){
            return response(['success'=>false , 'message'=>'User does not exist!'], 400);
        }

        $data = DB::table('subscription')->where('user_id', '=', $userId)->orderBy('id', 'desc')->get();
        $ticket_id = $this->getOrCreateTicket($userId);

        if(count($data) <= 0){
            return response([
                'success'=>false ,
                'message'=>'No subscription found!',
                'ticket_id'=> $ticket_id,
            ], 400);
        } else {
            // add checker for expiration
            $start_time = time();
            $end_time = $data[0]->end_time;

            if($start_time > $end_time){
                return response([
                    'success'=>false ,
                    'message'=>'Subscription expired!',
                    'ticket_id'=> $ticket_id,
                ], 400);
            } else {

                $data_plans = DB::table('plans')->where('plan_id', '=', $data[0]->plan_id)->get();

                return response([
                    'success'=>true ,
                    'message'=>'Currently subscribed',
                    'data'=>$data,
                    'plan'=>$data_plans,
                    'ticket_id'=> null,
                ], 200);
            }

        }
    }

    public function getOrCreateTicket($user_id){
        $data = DB::table('subscription_ticket')->where('user_id', '=', $user_id)->orderBy('id', 'desc')->get();
        if(count($data) <= 0){
            $generateToken = sha1(time());
            DB::table('subscription_ticket')->insert([
                'user_id' => $user_id,
                'ticket_id' => $generateToken,
            ]);
            return $generateToken;
        } else {
            return $data[0]->ticket_id;
        }
    }

    public function paymentSuccess(Request $request){
        $user_id = $request->user_id;
        $plan_id = $request->plan_id;
        $ticket_id = $request->ticket_id;

        $check_ticket = $this->paymentSuccessTicket($user_id, $ticket_id);

        if($check_ticket == false){
            return response(['success'=>false , 'message'=>'Invalid ticket!'], 400);
        } else {
            $check_plan = $this->paymentSuccessPlan($plan_id);
            if($check_plan === false){
                return response(['success'=>false , 'message'=>'Invalid plan selection!'], 400);
            } else {
                $total_months = $check_plan->total_months;
                $total_takes = $check_plan->takes;
                $start_date = date('Y-m-d H:i:s');
                $end_date = date('Y-m-d H:i:s', strtotime($start_date. $total_months.' month'));
                $start_time = time();
                $end_time = strtotime($end_date);

                $payload = [
                    "payment_token" => $ticket_id,
                    "plan_id" => $plan_id,
                    "user_id"   =>  $user_id,
                    "start_date"  =>  $start_date,
                    "end_date"    =>  $end_date,
                    "start_time"  => $start_time,
                    "end_time" => $end_time,
                    "current_takes" => $total_takes,
                    "total_takes" => $total_takes,
                ];

                DB::table('subscription')->insert($payload);
                DB::table('subscription_ticket')->where('user_id', '=', $user_id)->delete();

                $this->sendEmailLicense($payload);

                return response(['success'=>true , 'message'=>$payload], 200);
            }
        }
    }

    public function paymentSuccessTicket($user_id, $ticket_id){
        if(!$ticket_id){
            return false;
        } else {
            $data = DB::table('subscription_ticket')->where('user_id', '=', $user_id)->orderBy('id', 'desc')->get();
            if(count($data) <= 0){
                return false;
            } else {
                if($data[0]->ticket_id !== $ticket_id){
                    return false;
                } else {
                    return true;
                }
            }
        }
    }

    public function paymentSuccessPlan($plan_id){

        $data_plans = DB::table('plans')->where('plan_id', '=', $plan_id)->get();

        if(count($data_plans) <= 0){
            return false;
        } else {
            $row = [];
            foreach($data_plans as $item){
                $row = $item;
            }
            return $row;
        }
    }

    public function sendEmailLicense($payload){
        //plan data
        $data_plans = DB::table('plans')->where('plan_id', '=', $payload['plan_id'])->get();
        $length_description = $data_plans->first()->length_description;
        $amount_paid = $data_plans->first()->hkd;

        // user data
        $cuser = User::find($payload['user_id']);

        $data = array(
            'no' => $length_description,
            'email' => $cuser['email'],
            'username' => $cuser['username'],
            'transaction_id' => $payload['payment_token'],
            'amount_paid' => $amount_paid,
            'license_validity' => substr($payload['start_date'],0,10).' to '.substr($payload['end_date'],0,10)
        );
        
        Mail::send('mail_license', $data, function ($message) use ($data)  {
            $message->to($data['email'], $data['email'])->subject('Find Life Interest License');
            $message->from('cardgame@cardgame.com','Find Life Interest');
        });
    }

}
