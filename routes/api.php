<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\User;
use App\Http\Controllers\Accounts\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Accounts
Route::post('/register','Accounts\AuthController@register');
Route::post('/login','Accounts\AuthController@login');
Route::post('/verify','Accounts\AuthController@verify');
Route::post('/resendEmail','Accounts\AuthController@resendEmail');
//
// Route::post('/user/update_user','Accounts\AuthController@updateUser');
Route::post('/user/update_user', [ AuthController::class, 'updateUser' ])->name('image.upload.post');
Route::get('/user/{userId}','Accounts\AuthController@getUser');
Route::post('/user/change_password','Accounts\AuthController@changePassword');

//Plans
Route::get('/plans','Plans\PlansController@index');
Route::get('/plans/is_subscribed/{userId}','Plans\PlansController@isSubscribed');
Route::post('/plans/success','Plans\PlansController@paymentSuccess');

//Game
Route::post('/result','Game\GameController@result');
Route::get('/result/{resultId}','Game\GameController@getResults');
Route::get('/results/{userId}','Game\GameController@getAllResults');
Route::get('/result_last/{userId}','Game\GameController@getLastResults');

//Others
Route::post('/contact-us','Others\OthersController@contactUs');


//Others

Route::apiResource('/user', 'UserController');

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    return "Cache is cleared";
});
