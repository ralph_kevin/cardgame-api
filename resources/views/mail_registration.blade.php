<!DOCTYPE html>
<html>
<head>
<title></title>
<style>
    body {
        position: relative;
        width: 100%;
        max-width: 600px;
        min-height: 715px;
        margin: 0 auto;
        padding: 0;
        color: #4D4F5C;
    }
    body:before {
        content: '';
        position: absolute;
        top: 0;
        width: 100%;
        height: 90px;
        background-image: url('http://13.213.4.206/emails-templates/img/bg.png');
        background-size: cover;
        background-repeat: no-repeat;
    }
    body:after {
        content: '';
        position: absolute;
        bottom: 0;
        width: 100%;
        height: 90px;
        background-image: url('http://13.213.4.206/emails-templates/img/bg.png');
        background-size: cover;
        background-repeat: no-repeat;
        transform: rotate(180deg);
    }
    .wrap {
        padding: 0 50px;;
    }
    h1 {
        padding-top: 90px;
        text-align: center; color: #2F2E50; font-size: 23px; margin-bottom: 31px;
    }
    .name {
        margin-bottom: 20px; font-size: 12px; color: #4D4F5C;
    }
    .desc1 {
        margin-bottom: 20px; font-size: 12px; color: #4D4F5C;
    }
    .code {
        font-size: 24px;
        font-weight: bold;
    }
</style>
</head>
<body>
    <div class="wrap">
        <h1>Yay! You’ve successfully created your Find Life Interest account</h1>
        <p class="name">Congratulations!</strong></p>
        <p class="desc1">With Find Life Interest, you start by selecting cards that are most appealing to you. And as you go along the way, you’ll be able to have a deeper and better understanding of how you do things. </p>
        <p class="desc1">In the end, you’ll know your character type and will be given suggestions about the career path well-suited for you. </p>
    </div>
</body>
</html>