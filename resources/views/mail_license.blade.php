<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>
<style>
    body {
        position: relative;
        width: 100%;
        max-width: 600px;
        min-height: 715px;
        margin: 0 auto;
        padding: 0;
        color: #4D4F5C;
    }
    body:before {
        content: '';
        position: absolute;
        top: 0;
        width: 100%;
        height: 90px;
        background-image: url('http://13.213.4.206/emails-templates/img/bg.png');
        background-size: cover;
        background-repeat: no-repeat;
    }
    body:after {
        content: '';
        position: absolute;
        bottom: 0;
        width: 100%;
        height: 90px;
        background-image: url('http://13.213.4.206/emails-templates/img/bg.png');
        background-size: cover;
        background-repeat: no-repeat;
        transform: rotate(180deg);
    }
    .wrap {
        padding: 0 50px;;
    }
    h1 {
        padding-top: 90px;
        text-align: center; color: #2F2E50; font-size: 23px; margin-bottom: 31px;
    }
    .desc1 {
        margin-bottom: 20px; font-size: 12px; color: #4D4F5C;
    }
    h2 {
        font-size: 16px;
        color: #4D4F5C;
    }
    .item {
        font-size: 12px;
        color: #4D4F5C;
    }
    .item span {
        color: #00AEEF;
    }
</style>
</head>
<body>
    <div class="wrap">
        <h1>Yay! You’ve successfully paid for Find Life Interest {{ $no }}!</h1>
        <p class="desc1">You can now start discover the gem that you are. Enjoy the journey and always remember that we are cheering for you! :)</p>

        <h2>Purchase Details</h2>

        <p class="item">Username: <span>{{ $username }}</span></p>
        <p class="item">Transaction ID: <span>{{ $transaction_id }}</span></p>
        <p class="item">Amount Paid: <span>{{ $amount_paid }}HKD</span></p>
        <p class="item">License Validity: <span>{{ $license_validity }}</span></p>
    </div>
</body>
</html>